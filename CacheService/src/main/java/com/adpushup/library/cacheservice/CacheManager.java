package com.adpushup.library.cacheservice;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class CacheManager
{

    private static HashMap<Object, CachedObject> _cacheData = new HashMap();
    private static final Object LOCK = new Object();
    private static Integer _sleepTime;
    private static Integer _cacheSize;

    public static void setupCacheManager(Integer cacheSize, Integer refreshInterval)
    {
        _cacheSize = ((cacheSize == null) || (cacheSize == 0)) ? 1000 : cacheSize;
        _sleepTime = ((refreshInterval) == null || (refreshInterval == 0)) ? 300000 : refreshInterval;
    }

    static
    {
        try
        {

            Thread objectRemover = new Thread(new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        while (true)
                        {
                            Set keySet = _cacheData.keySet();
                            Iterator keys = keySet.iterator();
                            while (keys.hasNext())
                            {
                                Object key = keys.next();
                                CachedObject object = _cacheData.get(key);
                                if (object.isExpired())
                                {
                                    _cacheData.remove(key);
                                    _cacheSize--;
                                }
                            }

                            Thread.sleep(_sleepTime);
                        }
                    }
                    catch (Exception e)
                    {
                        System.out.println(e);
                    }
                }
            });
            objectRemover.setPriority(Thread.MIN_PRIORITY);
            objectRemover.start();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }

    public static void putCache(CachedObject object)
    {
        synchronized (LOCK)
        {
            if (isCacheFull())
            {
                Object keyToBeRemoved = getLFUObjectKey();
                _cacheData.remove(keyToBeRemoved);
            }

            _cacheData.put(object.getId(), object);
            _cacheSize++;
        }
    }

    public static CachedObject getObject(Object id)
    {
        synchronized (LOCK)
        {
            CachedObject object = (CachedObject) _cacheData.get(id);

            if (object == null)
            {
                return null;
            }
            if (object.isExpired())
            {
                _cacheData.remove(id);
                _cacheSize--;
                return null;
            }

            object.updateFrequency();
            return object;
        }
    }

    private static Boolean isCacheFull()
    {
        return _cacheData.size() >= _cacheSize;
    }

    private static Object getLFUObjectKey()
    {
        Object key = 0;
        Integer minFreq = 0;

        for (Map.Entry<Object, CachedObject> entry : _cacheData.entrySet())
        {
            Integer objectFrequency = entry.getValue().getFrequency();
            if (minFreq > objectFrequency)
            {
                key = entry.getKey();
                minFreq = objectFrequency;
            }
        }

        return key;
    }
}
