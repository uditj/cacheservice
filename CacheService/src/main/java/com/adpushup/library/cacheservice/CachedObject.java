package com.adpushup.library.cacheservice;

import java.util.Calendar;
import java.util.Date;

public class CachedObject
{

    private Date _dateofExpiration = null;
    private Object _id = null;
    private Object _object = null;
    private Integer _frequency = 0;

    public CachedObject(Object obj, Object id, int minutesToLive)
    {
        _object = obj;
        _id = id;
        if (minutesToLive != 0)
        {
            _dateofExpiration = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(_dateofExpiration);
            cal.add(Calendar.MINUTE, minutesToLive);
            _dateofExpiration = cal.getTime();
        }
    }

    public boolean isExpired()
    {
        if (_dateofExpiration != null)
        {
            return _dateofExpiration.before(new Date());
        }
        else
        {
            return false;
        }
    }

    public Object getId()
    {
        return _id;
    }

    public Object getObject()
    {
        return _object;
    }

    public Integer getFrequency()
    {
        return _frequency;
    }

    public void updateFrequency()
    {
        _frequency++;
    }

}
